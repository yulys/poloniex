import {FC} from 'react';

import styles from './Error.module.css';

const Error: FC = ({children}) => {
  return <div className={styles.toast}>{children}</div>
}

export default Error;
