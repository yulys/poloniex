import React, {useCallback, FC, memo} from 'react';
import classNames from 'classnames';

import {Ticker} from '@types';
import ObservableField from "@components/ObservableField/ObservableField";
import styles from './Table.module.css';

interface IProps {
  data: Ticker,
  active?: boolean,

  onClick(data: Ticker): void,
}

const Row: FC<IProps> = ({data, onClick, active}: IProps) => {
  const onRowClick = useCallback(() => {
    onClick(data);
  }, [data, onClick]);

  return (
    <tr key={data.id}
        className={classNames({
          [styles.active]: active
        })}
        onClick={onRowClick}>
      <td><ObservableField val={data.name}/></td>
      <td><ObservableField val={data.last}/></td>
      <td><ObservableField val={data.highestBid}/></td>
      <td><ObservableField val={data.percentChange}/></td>
    </tr>
  )
}

export default memo(Row);
