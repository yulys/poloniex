import React from 'react';
import classNames from 'classnames';

import usePrevious from '@hooks/usePrevious';
import styles from './ObservableField.module.css';

interface IProps {
  val: string | number
}

const ObservableField: React.FC<IProps> = ({val, children}) => {
  const prevVal = usePrevious(val);
  return <span className={classNames({
    [styles.text]: true,
    [styles.more]: prevVal > val,
    [styles.less]: prevVal < val,
  })}>{val}</span>;
}

export default ObservableField;
