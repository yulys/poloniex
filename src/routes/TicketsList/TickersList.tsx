import React, {useCallback, useState, useEffect, useRef} from 'react';
import {inject, observer} from 'mobx-react';

import Table from '@components/Table/Table';
import Error from '@components/Error/Error';
import {Ticker} from '@types';
import ModalInfo from '@components/ModalInfo/ModalInfo';

interface IProps {
  store: {
    tickers: Array<Ticker>,
    isLoading: boolean,
    error: any,
    fetchTickers(showLoader?: boolean): Promise<Ticker>;
  }
}

const FETCH_INTERVAL = 5000;

const TickersList: React.FC<IProps> = ({store}) => {
  const [activeRow, setActiveRow] = useState<Ticker | null>(store.tickers.length ? store.tickers[0] : null);
  const [isModalOpened, setIsModalOpened] = useState(false);
  const intervalId = useRef<any>();

  const onRowClick = useCallback((ticker: Ticker) => {
    setIsModalOpened(true);
    setActiveRow(ticker);
  }, []);

  const onModalClose = useCallback(() => {
    setIsModalOpened(false);
    setActiveRow(null);
  }, []);

  const fetchData = useCallback((showLoader: boolean) => {
    store.fetchTickers(showLoader).then(() => {
      clearTimeout(intervalId.current);
      intervalId.current = setTimeout(() => {
        fetchData(false);
      }, FETCH_INTERVAL);
    });
  }, [store]);

  useEffect(() => {
    fetchData(true);
    return () => clearTimeout(intervalId.current);
  }, []);


  useEffect(() => {
    if (isModalOpened) {
      fetchData(false);
    }
    return () => clearTimeout(intervalId.current)
  }, [isModalOpened, fetchData]);

  return (
    <>
      <Table data={store.tickers}
             loading={store.isLoading}
             activeRowData={activeRow}
             onRowClick={onRowClick}/>
      {store.error && <Error>I have a problem</Error>}
      <ModalInfo open={isModalOpened} onClose={onModalClose} data={activeRow}/>
    </>
  );
}

export default inject('store')(observer(TickersList));
