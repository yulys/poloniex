import {action, makeAutoObservable, observable} from 'mobx';
import axios from 'axios';
import {Ticker, IUserStore} from '@types';

const API_ROOT = 'https://poloniex.com/public';


class TickerInstance{
  @observable name = '';
  @observable baseVolume = ''
  @observable high24hr = ''
  @observable highestBid = ''
  @observable id = 1
  @observable isFrozen = ''
  @observable last = ''
  @observable low24hr = ''
  @observable lowestAsk = ''
  @observable percentChange = ''
  @observable postOnly = ''
  @observable quoteVolume = ''

  constructor(ticker: Ticker) {
    makeAutoObservable(this);
    this.name = ticker.name;
    this.baseVolume = ticker.name;
    this.high24hr = ticker.high24hr;
    this.highestBid = ticker.high24hr;
    this.id = ticker.id;
    this.isFrozen = ticker.isFrozen;
    this.last = ticker.last;
    this.low24hr = ticker.low24hr;
    this.lowestAsk = ticker.lowestAsk;
    this.percentChange = ticker.percentChange;
    this.postOnly = ticker.postOnly;
    this.quoteVolume = ticker.quoteVolume;
  }
}


class ObservableStore implements IUserStore {
  @observable tickers: Array<TickerInstance> = [];
  @observable isLoading: boolean = false;
  @observable error: any = null;

  constructor() {
    makeAutoObservable(this);
  }

  @action.bound
  setIsLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  };

  @action.bound
  setError(error: any) {
    this.error = error;
  };

  @action.bound
  fetchTickers(showLoader: boolean) {
    if (showLoader) {
      this.setIsLoading(true);
    }

    return axios.get(`${API_ROOT}?command=returnTicker`)
      .then(({data}: any) => {

        this.setError(null);
        if (data.error) {
          throw Error(JSON.stringify(data));
        }

        this.tickers =
          Object
            .keys(data)
            .reduce((acc: Array<Ticker>, item) => {
              const tickerInstance = new TickerInstance({
                ...data[item],
                name: item
              });
              /*acc.push({
                ...data[item],
                name: item
              })*/
              acc.push(tickerInstance);
              return acc;
            }, []);
      })
      .catch((error) => {
        console.log('Error occured: ', error);
        this.setError(error);
      })
      .finally(() => {
        if (showLoader) {
          this.setIsLoading(false);
        }
      });
  }
}

const observableStore = new ObservableStore();
export default observableStore;
