export interface IUserStore {
  tickers: Array<Ticker>;
  isLoading: boolean;
  error: string;
}

export type Ticker = {
  name: string,
  baseVolume: string
  high24hr: string
  highestBid: string
  id: number
  isFrozen: string
  last: string
  low24hr: string
  lowestAsk: string
  percentChange: string
  postOnly: string
  quoteVolume: string
}
